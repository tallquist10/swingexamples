package inClassExamples;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class Swing2017 {
	private JTextArea jta;
	private JTextField outField;

	public static void main(String[] args) {
		new Swing2017();
	}

	public Swing2017() {
		JFrame frame = new JFrame("Dumb application");
		Container cont = frame.getContentPane();
		cont.setLayout(new BorderLayout());

		JButton button = new JButton("Push me");
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(button);
		cont.add(buttonPanel, BorderLayout.NORTH);

		jta = new JTextArea(20, 80);
		cont.add(jta, BorderLayout.CENTER);
		jta.setBorder(new TitledBorder("Input"));

		outField = new JTextField(80);
		cont.add(outField, BorderLayout.SOUTH);
		outField.setBorder(new TitledBorder("Output"));

		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int length = jta.getText().length();
				outField.setText("Length is " + length);
			}
		});
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.pack();
		frame.setSize(400, 400);
		frame.setVisible(true);
	}

}
